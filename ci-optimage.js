(function () {
  'use strict'

  var images = document.querySelectorAll('img[src*="cloudimg.io"]'),
      dpr = window.devicePixelRatio || 1;

  images = Array.prototype.slice.call(images);

  function parseSrc(src) {
      src = src.split('http');
      var ciUrl = src[1].split('/').slice(3, 6);
      var params = {
        dest: 'http' + src[src.length - 1],
        func: ciUrl[0],
        param: ciUrl[1],
        filters: ciUrl[2].split('.')
      };
      return params;
  }

  images.forEach(function (image) {
      setTimeout(function () {
          var params = parseSrc(image['src']);
          image['src'] = 'http://demo.cloudimg.io/' +
          params.func + '/' + (dpr * params.param) + '/q' + Math.round(95/dpr-20) + '/' + params.dest;
      }, 100);
  });
})();
